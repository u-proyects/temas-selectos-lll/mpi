//
//  Image.cpp
//
//  Created by
//      Francisco Ramos  157330
//      Aldair González  158020
//      Mariella Sánchez 157130
//      Karla Martínez   156758
//  on 13/03/21.

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <iostream>
#include <vector>
#include <chrono>
#include <omp.h>
#include <mpi.h>

#include "Pixel.cpp"

using namespace std;
using namespace std::chrono;

class Image
{
public:
    int width, height, channels;
    unsigned char *originalData;
    vector<vector<Pixel>> image;

    Image(const char *path)
    {
        // cout << "Loading image at '" << path << "' ..." << endl;
        originalData = stbi_load(path, &width, &height, &channels, STBI_rgb);

        if (stbi_failure_reason())
        {
            cout << stbi_failure_reason();
            exit(1);
        }
        // else{
        //     cout << "Image loaded.\n"<< endl;
        // }

        image.resize(width, vector<Pixel>(height));

        createMatrixOfPixels();
    }

    ~Image()
    {
        stbi_image_free(originalData);
    }

    void createMatrixOfPixels()
    {
        int k = 0;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {

                int red = (int)originalData[k];
                int green = (int)originalData[k + 1];
                int blue = (int)originalData[k + 2];

                Pixel pixel = Pixel(red, green, blue);

                image[i][j] = pixel;

                k += 3;
            }
        }
    }

    unsigned char *getDataArray()
    {
        unsigned char *localData = new unsigned char[width * height * 3];

        int k = 0;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {

                int red = image[i][j].red;
                int green = image[i][j].green;
                int blue = image[i][j].blue;

                localData[k] = (char)red;
                localData[k + 1] = (char)green;
                localData[k + 2] = (char)blue;

                k += 3;
            }
        }

        return localData;
    }

    void saveAsJPG(const char *path)
    {
        cout << "Saving image at '" << path << "'..." << endl;
        unsigned char *localData = getDataArray();
        stbi_write_jpg(path, width, height, channels, localData, width * sizeof(int));
        cout << "Image saved" << endl;
    }

    vector<vector<vector<Pixel>>> chunks(int nchunks){

        /* Create the new result vector of chunks */
        vector<vector<vector<Pixel>>> result;

        /* Get the total of pixels */
        int totalPixels = width * height;
        
        /* Total of pixels / # of chunks */
        int partitionSize = totalPixels / nchunks;

        /* Dimension of the chunks */
        int dimension = sqrt(partitionSize);

        /* count */
        int count = 0;

        int lastI = 0;
        int lastJ = 0;

        /* Move through all the image */
        for (int i = 0; i < (width - dimension + 1); i+= (dimension -1) ){
            for (int j = 0; j < (height - dimension + 1); j+= (dimension -1)){

                /* Set the values */
                lastI = i;
                lastJ = j;

                /* Create the chunk  */
                vector<vector<Pixel>> chunk(dimension, vector<Pixel>(dimension));

                /* Move through all chunk */
                for (int x = 0; x < dimension; x++){                  
                    for (int y = 0; y < dimension; y++){

                        /* New I and J to extract the value of the image */
                        int newI = i + x;
                        int newJ = j + y;

                        /* Get the pixel */
                        Pixel pixel = image[newI][newJ];

                        /* Set pixel in chunk */
                        chunk[x][y] = pixel;
                    }
                }

                /* Adding the chunk */
                result.push_back(chunk);
            }
        }


        if(result.size() < nchunks){
            /* Create the last smaller chunk  */
            vector<vector<Pixel>> chunk(dimension, vector<Pixel>(dimension));

            /* Move through all chunk */
            for (int x = 0; x < dimension; x++){                  
                for (int y = 0; y < dimension; y++){

                    /* New I and J to extract the value of the image */
                    int newI = lastI + x;
                    int newJ = lastJ + y;

                    
                    /* Get the pixel */
                    Pixel pixel;
                    if(newI >= width || newJ >= height){
                        pixel = image[newI][newJ];
                    }else{
                        pixel = Pixel(0, 0, 0);
                    }
                    

                    /* Set pixel in chunk */
                    chunk[x][y] = pixel;
                }
            }

            /* Adding the chunk */
            result.push_back(chunk);
        }

        return result;

    }

    double normalConvolution(double kernel[3][3])
    {
        /* Create the new result image */
        vector<vector<Pixel>> resultImage(width, vector<Pixel>(height));

        /* start timer */
        auto start = high_resolution_clock::now();

        /* -----Convolution----- */

        /* Move through all the image */
        for (int i = 0; i < width - 3; i++)
        {
            for (int j = 0; j < height - 3; j++)
            {

                /* Accumulators  */
                int sumRed = 0;
                int sumGreen = 0;
                int sumBlue = 0;

                /* Move through all the kernel */
                for (int x = 0; x < 3; x++)
                {
                    for (int y = 0; y < 3; y++)
                    {

                        /* New I and J to extract the value of the image */
                        int newI = i + x;
                        int newJ = j + y;

                        /* Extract the value of the image pixel */
                        int red = image[newI][newJ].red;
                        int green = image[newI][newJ].green;
                        int blue = image[newI][newJ].blue;

                        /* Multiplying by its corresponding part on the kernel */
                        int newRed = red * kernel[x][y];
                        int newGreen = green * kernel[x][y];
                        int newBlue = blue * kernel[x][y];

                        /* Adding to the accumulator */
                        sumRed += newRed;
                        sumGreen += newGreen;
                        sumBlue += newBlue;
                    }
                }

                /* Setting the values in the new image */
                resultImage[i][j].red = sumRed;
                resultImage[i][j].green = sumGreen;
                resultImage[i][j].blue = sumBlue;
            }
        }
        /* -----End-Convolution----- */

        image = resultImage;

        /* finish timer and print duration */
        auto finish = high_resolution_clock::now();
        duration<double> duration = finish - start;
        // cout << "Normal convolution time: " << duration.count() << "s\n";
        return duration.count();
    }

    double ompParallelConvolution(double kernel[3][3])
    {
        /* Create the new result image */
        vector<vector<Pixel>> resultImage(width, vector<Pixel>(height));

        /* start timer */
        auto start = high_resolution_clock::now();

        /* -----Convolution----- */
        #pragma omp parallel for collapse(2)
        /* Move through all the image */
        for (int i = 0; i < width - 3; i++)
        {
            for (int j = 0; j < height - 3; j++)
            {

                /* Accumulators  */
                int sumRed = 0;
                int sumGreen = 0;
                int sumBlue = 0;

                /* Move through all the kernel */

                for (int x = 0; x < 3; x++)
                {
                    for (int y = 0; y < 3; y++)
                    {

                        /* New I and J to extract the value of the image */
                        int newI = i + x;
                        int newJ = j + y;

                        /* Extract the value of the image pixel */
                        int red = image[newI][newJ].red;
                        int green = image[newI][newJ].green;
                        int blue = image[newI][newJ].blue;

                        /* Multiplying by its corresponding part on the kernel */
                        int newRed = red * kernel[x][y];
                        int newGreen = green * kernel[x][y];
                        int newBlue = blue * kernel[x][y];

                        /* Adding to the accumulator */
                        sumRed += newRed;
                        sumGreen += newGreen;
                        sumBlue += newBlue;
                    }
                }

                /* Setting the values in the new image */
                resultImage[i][j].red = sumRed;
                resultImage[i][j].green = sumGreen;
                resultImage[i][j].blue = sumBlue;
            }
        }
        /* -----End-Convolution----- */

        image = resultImage;

        /* finish timer and print duration */
        auto finish = high_resolution_clock::now();
        duration<double> duration = finish - start;
        // cout << "Parallel convolution time: " << duration.count() << "s\n";
        return duration.count();
    }

    double normalConvolution(double kernel[3][3], vector<vector<Pixel>> img)
    {
        /* Define width and height */
        int imgW = img[0].size();
        int imgH = img.size();

        /* Create the new result image */
        vector<vector<Pixel>> resultImage(imgW, vector<Pixel>(imgH));

        /* start timer */
        auto start = high_resolution_clock::now();

        /* -----Convolution----- */

        /* Move through all the image */
        for (int i = 0; i < imgW - 3; i++)
        {
            for (int j = 0; j < imgH - 3; j++)
            {

                /* Accumulators  */
                int sumRed = 0;
                int sumGreen = 0;
                int sumBlue = 0;

                /* Move through all the kernel */
                for (int x = 0; x < 3; x++)
                {
                    for (int y = 0; y < 3; y++)
                    {

                        /* New I and J to extract the value of the image */
                        int newI = i + x;
                        int newJ = j + y;

                        /* Extract the value of the image pixel */
                        int red   = img[newI][newJ].red;
                        int green = img[newI][newJ].green;
                        int blue  = img[newI][newJ].blue;

                        /* Multiplying by its corresponding part on the kernel */
                        int newRed   = red   * kernel[x][y];
                        int newGreen = green * kernel[x][y];
                        int newBlue  = blue  * kernel[x][y];

                        /* Adding to the accumulator */
                        sumRed   += newRed;
                        sumGreen += newGreen;
                        sumBlue  += newBlue;
                    }
                }

                /* Setting the values in the new image */
                resultImage[i][j].red   = sumRed;
                resultImage[i][j].green = sumGreen;
                resultImage[i][j].blue  = sumBlue;
            }
        }
        /* -----End-Convolution----- */

        img = resultImage;

        /* finish timer and print duration */
        auto finish = high_resolution_clock::now();
        duration<double> duration = finish - start;
        
        // cout << "Normal convolution time: " << duration.count() << "s\n";
        return duration.count();
    }

    void revertMatrixToOriginalImg()
    {
        createMatrixOfPixels();
    }
};