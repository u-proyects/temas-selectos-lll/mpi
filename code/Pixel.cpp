//
//  Pixel.cpp
//
//  Created by
//      Francisco Ramos  157330
//      Aldair González  158020   
//      Mariella Sánchez 157130
//      Karla Martínez   156758
//  on 13/03/21.

#include <iostream>

using namespace std;

class Pixel {
public:
    int red, green, blue;
    
    Pixel(){}
    
    Pixel(int r, int g, int b){
        red   = r;
        green = g;
        blue  = b;
    }
    
    void print(){
        cout << "(";
        cout << red;
        cout << ",";
        cout << green;
        cout << ",";
        cout << blue;
        cout << ")";
    }

};
