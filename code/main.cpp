//
//  main.cpp
//
//  Created by
//      Francisco Ramos  157330
//      Aldair González  158020
//      Mariella Sánchez 157130
//      Karla Martínez   156758
//  on 13/03/21.

#include <iostream>
#include <iomanip>
#include <omp.h>

#include "Image.cpp"
// #include "Pixel.cpp"

using namespace std;

int main(int argc, char *argv[])
{
    /* Set the selected size of the image */
    string selectedSize = argv[1];

    /* Init MPI */
    MPI_Status status;
    int rank, size;
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* Set the paths for loading and saving the images */
    string loadPath = "../images/";
    // string savePath = "../../results/";

    /* Set the name of the images */
    string loadNames[3] = {
        "lena.jpg",
        "lena2x.jpg",
        "lena3x.jpg"};

    /* Set the default image  */
    string loadName = loadNames[0];

    if(selectedSize == "small"){
        loadName = loadNames[0];
    }
    if(selectedSize == "medium"){
        loadName = loadNames[1];
    }

    if(selectedSize == "large"){
        loadName = loadNames[2];
    }


    /* Create the images objects with the paths to the images */
    Image lena = Image((loadPath + loadName).c_str());

    /* Defining kernel (Box blur) */
    double kernel[3][3] = {
        {0.111, 0.111, 0.111},
        {0.111, 0.111, 0.111},
        {0.111, 0.111, 0.111},
    };

    if (rank == 0)
    {
        /* Get chunks */
        vector<vector<vector<Pixel>>> chunks = lena.chunks(size);

        /* start timer */
        auto start = high_resolution_clock::now();

        /* Move through all chunks */
        for(int i = 1; i<chunks.size(); i++){
            /* Send chunk size */
            int chunkWidth  = chunks[i][0].size();
            int chunkHeight = chunks[i].size();

            // cout << "Sending " << chunkWidth << " to " << i << endl;
            MPI_Send(&chunkWidth,  1, MPI_INT, i, 0, MPI_COMM_WORLD);

            // cout << "Sending " << chunkHeight << " to " << i << endl;
            MPI_Send(&chunkHeight, 1, MPI_INT, i, 0, MPI_COMM_WORLD);

            // cout << endl;

            /* Move through all chunk */
            for(int j=0; j<chunkWidth; j++){
                for(int k=0; k<chunkHeight; k++){
                    /* Send pixel data */
                    Pixel pixel = chunks[i][j][k];

                    int red   = pixel.red;
                    int green = pixel.red;
                    int blue  = pixel.red;

                    MPI_Send(&red,   1, MPI_INT, i, 0, MPI_COMM_WORLD);
                    MPI_Send(&green, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                    MPI_Send(&blue,  1, MPI_INT, i, 0, MPI_COMM_WORLD);
                }
            }

        }

        /* Define var to accumulate avg time */
        double avgtime = 0;

        /* Move through all nodes */
        for(int i = 1; i<size; i++){

            /* Receive data from nodes */
            double time;

            MPI_Recv(&time, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            avgtime += time;
        }

        avgtime += lena.normalConvolution(kernel, chunks[0]);
        avgtime = avgtime/size;

        // cout << avgtime << endl;

        /* finish timer and print duration */
        auto finish = high_resolution_clock::now();
        duration<double> duration = finish - start;
        cout << fixed << setprecision(10);
        cout << "Parallel convolution time: " << duration.count() << "s\n";
        cout << endl;
    }
    else
    {
        /* Receive chunk size */
        int chunkWidth;
        int chunkHeight;

        MPI_Recv(&chunkWidth,  1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&chunkHeight, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        /* Create chunk */
        vector<vector<Pixel>> chunk(chunkWidth, vector<Pixel>(chunkHeight));

        /* Move through all chunk */
        for(int i=0; i<chunkWidth; i++){
            for(int j=0; j<chunkHeight; j++){

                /* Receive pixel data */
                int red;
                int green;
                int blue;

                MPI_Recv(&red,   1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&green, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(&blue,  1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                /* Create pixel object */
                Pixel pixel= Pixel(red, green, blue);

                /* Add pixel to chunk */
                chunk[i][j] = pixel;
            }
        }


        /* Convolution in chunk */
        double time = lena.normalConvolution(kernel, chunk);

        /* Send result */
        MPI_Send(&time, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    /* End MPI */
    MPI_Finalize();

    return 0;
}

