clear
# mpiCC main.cpp -o out/main

mpirun --mca btl ^openib -np 5 out/main small

# mpirun --mca btl ^openib -np 10 out/main medium
# mpirun --mca btl ^openib -np 10 out/main large


# echo "5 NODEs M"
# for i in 1 2 3 4 5 7 8 9 10
# do
#   mpirun --mca btl ^openib -np 5 out/main medium
# done

# echo "10 NODEs M"
# for i in 1 2 3 4 5 7 8 9 10
# do
#   mpirun --mca btl ^openib -np 10 out/main medium
# done


# echo "5 NODEs M"
# for i in 1 2 3 4 5 7 8 9 10
# do
#   mpirun --mca btl ^openib -np 5 out/main large
# done

# echo "10 NODEs M"
# for i in 1 2 3 4 5 7 8 9 10
# do
#   mpirun --mca btl ^openib -np 10 out/main large
# done


